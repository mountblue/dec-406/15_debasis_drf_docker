from .models import Matches, Deliveries
from django.contrib.auth.models import User
from .serializers import MatchesSerializer, DeliveriesSerializer, UserSerializer
from rest_framework.pagination import PageNumberPagination
from rest_framework import mixins, generics
from rest_framework import permissions



class ResultSetPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 1000


class MatchList(generics.ListCreateAPIView):
    queryset = Matches.objects.all()
    serializer_class = MatchesSerializer
    pagination_class = ResultSetPagination
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class MatchDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Matches.objects.all()
    serializer_class = MatchesSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class DeliveriesList(generics.ListCreateAPIView):
    queryset = Deliveries.objects.all()
    serializer_class = DeliveriesSerializer
    pagination_class = ResultSetPagination
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class DeliveriesDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Deliveries.objects.all()
    serializer_class = DeliveriesSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer