from rest_framework import serializers
from .models import Matches, Deliveries
from django.contrib.auth.models import User

class MatchesSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    class Meta:
        model = Matches
        fields = ('id','season','city','date','team1','team2','toss_winner', 'toss_decision','result','dl_applied','winner','win_by_wickets', 'player_of_match', 'owner', 'url')


class DeliveriesSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    match = serializers.HyperlinkedRelatedField(
        many = False,
        view_name = 'match-detail',
        lookup_url_kwarg = 'pk',
        read_only = True
    )

    class Meta:
        model = Deliveries
        fields = ('match', 'inning', 'batting_team', 'bowling_team', 'over', 'ball', 'batsman', 'non_striker', 'bowler', 'is_super_over', 'wide_runs', 'bye_runs', 'legbye_runs','noball_runs', 'penalty_runs', 'batsman_run','extra_runs', 'total_runs', 'player_dismissed',
        'dismissal_kind','fielder', 'owner', 'url')

class UserSerializer(serializers.ModelSerializer):
    matches = serializers.PrimaryKeyRelatedField(many=True, queryset= Matches.objects.all())
    deliveries = serializers.PrimaryKeyRelatedField(many=True, queryset= Deliveries.objects.all())
    class Meta:
        model = User
        fields = ('id', 'username', 'matches', 'deliveries')