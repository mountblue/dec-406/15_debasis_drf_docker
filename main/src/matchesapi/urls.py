from django.contrib import admin
from django.urls import path, include
from matchesapi import views

urlpatterns = [
    path('api/matches/', views.MatchList.as_view()),
    path('api/deliveries/', views.DeliveriesList.as_view()),
    path('api/deliveries/<int:pk>', views.DeliveriesDetail.as_view()),
    path('api/matches/<int:pk>', views.MatchDetail.as_view() , name='match-detail'),
    path('users/', views.UserList.as_view()),
    path('users/<int:pk>/', views.UserDetail.as_view()),
    path('admin/', admin.site.urls),
]

urlpatterns += [
    path('api-auth/', include('rest_framework.urls')),
]